package com.mj.myapplication.viewmodel

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mj.myapplication.services.model.UserModel
import com.mj.myapplication.services.repository.ProjectRepository
import com.mj.myapplication.services.repository.Response
import kotlinx.coroutines.launch

@RequiresApi(Build.VERSION_CODES.M)
class UserViewModel(val repository: ProjectRepository) :ViewModel() {

    init {
        viewModelScope.launch {
            repository.getuserData()
        }

    }
    val userLiveData: LiveData<Response<UserModel>>
        get() = repository.userLiveData

}