package com.mj.myapplication.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mj.myapplication.db.UserDataBase
import com.mj.myapplication.services.repository.ProjectRepository

class UserViewModelFactory(val repository: ProjectRepository):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return UserViewModel(repository) as T
    }
}