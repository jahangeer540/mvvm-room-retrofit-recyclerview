package com.mj.myapplication.db

import androidx.room.Database
import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.mj.myapplication.services.model.UserModelItem
// update version number whe we change ant db related changes
// like new coloum,table inserted are removed etc
@Database(entities = [UserModelItem::class], version =2 )
abstract class UserDataBase : RoomDatabase() {
    abstract fun getNotesDao(): UserDao

    companion object {
        // migration from version 1 to 2
        val migration1_2=object :Migration(1,2){
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE UserTable ADD COLUMN table_id INTEGER NOT NULL DEFAULT (1) ")
            }

        }

        // Singleton prevents multiple
        // instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: UserDataBase? = null

        fun getDatabase(context: Context): UserDataBase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    UserDataBase::class.java,
                    "note_database"
                ).addMigrations(migration1_2)
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}