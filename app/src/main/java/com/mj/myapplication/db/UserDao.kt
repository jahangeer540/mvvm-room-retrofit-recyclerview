package com.mj.myapplication.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mj.myapplication.services.model.UserModelItem


@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend  fun insertData(order: List<UserModelItem?>?)


    @Query("Select * From usertable")
    suspend fun getAllUserList(): List<UserModelItem>

}