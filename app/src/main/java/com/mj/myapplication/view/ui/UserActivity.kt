package com.mj.myapplication.view.ui

import android.content.ContentValues.TAG
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mj.myapplication.application.UserApplication
import com.mj.myapplication.databinding.ActivityUserBinding
import com.mj.myapplication.services.repository.Response
import com.mj.myapplication.view.adapters.UserAdapter
import com.mj.myapplication.viewmodel.UserViewModel
import com.mj.myapplication.viewmodel.UserViewModelFactory

class UserActivity : AppCompatActivity() {
    // view binding
    lateinit var _userBinding: ActivityUserBinding
    val userBinding get() = _userBinding

    private lateinit var rvUserAdapter: UserAdapter

    lateinit var userViewModel: UserViewModel

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _userBinding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(userBinding.root)
        //application class
        val repository = (application as UserApplication).repository

        // create  layoutManager
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        // pass it to rvLists layoutManager
        userBinding.recyclerUser.setLayoutManager(layoutManager)

        userViewModel =
            ViewModelProvider(this, UserViewModelFactory(repository)).get(UserViewModel::class.java)
        userViewModel.userLiveData.observe(this, Observer {
            when (it) {
                is Response.Loading -> {
                }
                is Response.Success -> {
                    Log.d(TAG, "onCreate: " + it)
                    // initialize the adapter,
                    // and pass the required argument
                    it.data?.let {
                        rvUserAdapter = UserAdapter(it)
                    }
                    // attach adapter to the recycler view
                    userBinding.recyclerUser.adapter = rvUserAdapter
                }
                is Response.Error -> {
                    Toast.makeText(this, it.errorMessage1, Toast.LENGTH_LONG).show()
                }
                else -> {}
            }


        })
    }
}