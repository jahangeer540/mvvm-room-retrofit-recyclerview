package com.mj.myapplication.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mj.myapplication.databinding.ItemUserDataBinding
import com.mj.myapplication.services.model.UserModel

class UserAdapter(var userModel: UserModel) : RecyclerView.Adapter<UserAdapter.UserViewHolder>() {
// create an inner class with name ViewHolder
    // It takes a view argument, in which pass the generated class of single_item.xml
    // ie SingleItemBinding and in the RecyclerView.ViewHolder(binding.root) pass it like this

    inner class UserViewHolder(val binding: ItemUserDataBinding) :
        RecyclerView.ViewHolder(binding.root)

    // inside the onCreateViewHolder inflate the view of SingleItemBinding
    // and return new ViewHolder object containing this layout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val binding =
            ItemUserDataBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return UserViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        with(holder) {
            with(userModel[position]){
                   binding.txtUserId.text=this.userId.toString()
                   binding.txtId.text=this.id.toString()
                   binding.txtTitle.text=this.title
                   binding.txtBody.text=this.body

            }
        }
    }

    override fun getItemCount(): Int {
        return userModel.size
    }
}