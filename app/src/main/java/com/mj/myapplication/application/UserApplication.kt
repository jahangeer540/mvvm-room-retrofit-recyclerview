package com.mj.myapplication.application

import android.app.Application
import com.mj.myapplication.db.UserDataBase
import com.mj.myapplication.services.repo.RetrofitClient
import com.mj.myapplication.services.repository.ProjectRepository

class UserApplication : Application() {
    lateinit var repository: ProjectRepository
    override fun onCreate() {
        super.onCreate()
        initialize()
    }

    private fun initialize() {
        val retrofitClient = RetrofitClient.getInstance()
        val db= UserDataBase.getDatabase(applicationContext)
         repository= ProjectRepository(retrofitClient,db,applicationContext)

    }
}