package com.mj.myapplication.services.repository

import android.content.Context
import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mj.myapplication.db.UserDataBase
import com.mj.myapplication.networkutils.NetworkUtils
import com.mj.myapplication.services.api.RetrofitService
import com.mj.myapplication.services.model.UserModel


class ProjectRepository(
    private val retrofitService1: RetrofitService,
    private val db: UserDataBase,
    val applicationContext: Context
) {
    val userMutableLiveData = MutableLiveData<Response<UserModel>>()
    val userLiveData: LiveData<Response<UserModel>>
        get() = userMutableLiveData

    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun getuserData() {
        if (NetworkUtils.isOnline(applicationContext)) {
            val result = retrofitService1.getAllMovies()

            try {
                if (result.body() != null) {
                    db.getNotesDao().insertData(result.body())
                    userMutableLiveData.value =Response.Success(result.body(),result.message())
                }else{
                    Toast.makeText(applicationContext, "something went Wrong", Toast.LENGTH_SHORT).show()
                }
            }catch ( e:Exception){
                userMutableLiveData.value=Response.Error(result.message())
                Toast.makeText(applicationContext, "something went Wrong"+e.message, Toast.LENGTH_SHORT).show()

            }

        }else{
            try{
                val userModel=UserModel()
                userModel.addAll(db.getNotesDao().getAllUserList())
                userMutableLiveData.value = Response.Success(userModel,"")
            }catch (e:Exception){
                Toast.makeText(applicationContext, "something went Wrong"+e.message, Toast.LENGTH_SHORT).show()

            }

        }
    }


}
