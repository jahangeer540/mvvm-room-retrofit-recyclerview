package com.mj.myapplication.services.api

import com.mj.myapplication.services.model.UserModel
import retrofit2.Response
import retrofit2.http.GET

interface RetrofitService {
    @GET("posts")
    suspend fun getAllMovies() : Response<UserModel>
}