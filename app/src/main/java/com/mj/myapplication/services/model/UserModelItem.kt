package com.mj.myapplication.services.model


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
@Entity(tableName = "UserTable")
data class UserModelItem(
    @SerializedName("body")
    @ColumnInfo(name = "body")
    val body: String,

    @ColumnInfo(name = "table_id")
    val table_id: Int,

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    @ColumnInfo(name = "id")
    val id: Int,
    @SerializedName("title")
    @ColumnInfo(name = "title")
    val title: String,
    @SerializedName("userId")
    @ColumnInfo(name = "userId" )
    val userId: Int
)