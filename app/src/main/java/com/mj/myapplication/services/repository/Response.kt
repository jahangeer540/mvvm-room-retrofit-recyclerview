package com.mj.myapplication.services.repository

//sealed class Response(val errorMessage: String?=null,
//                      val passmessage: String?=null,
//                      val data: UserModel?=null){
//    class Loading:Response()
//    class Success ( responce :UserModel ,val message:String):Response(data = responce, passmessage = message)
//    class Failed (val errorMessage1:String):Response(errorMessage=errorMessage1)
//}

sealed class Response<T>(val errorMessage: String?=null,
                      val passmessage: String?=null,
                      val data: T?=null){
    class Loading<T>:Response<T>()
    class Success<T> (val response :T? =null ,val message:String):Response<T>(data = response, passmessage = message)
    class Error <T>(val errorMessage1:String):Response<T>(errorMessage=errorMessage1)
}